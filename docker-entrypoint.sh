#!/usr/bin/env bash
set -Eeo pipefail

echo "$@"

CONFIG_FILE="$1"
if [ -z "$CONFIG_FILE" ]; then
    CONFIG_FILE="$PATRONICTL_CONFIG_FILE"
fi

DATA_DIR="$PATRONI_POSTGRESQL_DATA_DIR"
if [ -z "$DATA_DIR" ]; then
    DATA_DIR="$(/venv/bin/python3 -c 'import yaml; print(yaml.safe_load(open("'$CONFIG_FILE'").read())["postgresql"]["data_dir"])')"
fi

chmod 0700 "$DATA_DIR"
exec /venv/bin/patroni "$@"
