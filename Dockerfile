FROM timescale/timescaledb:latest-pg13 as base
FROM base as builder

ENV PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1

RUN apk add --no-cache \
    gcc \
    g++ \
    libffi-dev \
    openssl-dev \
    musl-dev \
    libstdc++ \
    python3-dev \
    py3-virtualenv \
    && python3 -m venv /venv \
    && /venv/bin/pip install wheel \
    && /venv/bin/pip install psycopg2-binary \
    && /venv/bin/pip install patroni[consul]

FROM base as final
RUN apk add --no-cache python3
COPY --from=builder /venv /venv
COPY docker-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
USER postgres
